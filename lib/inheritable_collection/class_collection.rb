module InheritableCollection

  # クラスを格納するコレクションクラス
  # @note 基底クラスのサブクラスのみ格納可能
  class ClassCollection < BasicCollection

    # メンバーを取得
    # @note 引数が基底クラスのサブクラスの場合、コレクションに含まれていなくても返す
    def [](arg)
      if arg.is_a?(Class) && arg <= klass
        arg
      else
        super
      end
    end

    # メンバーを追加
    # @raise [TypeError] 基底クラスのサブクラス以外を追加しようとした場合に発生
    def <<(member)
      raise TypeError, "#{klass.name}のサブクラスでなければなりません。 #{member.inspect}" unless member.is_a?(Class) && klass > member
      super(member)
    end

    # fromを継承してメンバーを作成
    # @param [String,nil] key 作成するメンバーのキー
    # @param [Member] from 継承元のメンバーもしくはそのキー
    # @return [Member] 作成したメンバー
    def _inherit(key = nil, from: klass, &block)
      self[from].inherits_to(key, &block)
    end
  end
end

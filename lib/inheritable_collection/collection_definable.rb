module InheritableCollection
  module CollectionDefinable

    # クラスコレクションを返すメソッドを生成
    # @param [Symbol] method_name 生成するメソッド名
    # @param [Class] klass コレクションの基底クラス
    # @param [Config] config コレクションの設定オブジェクト
    def def_class_collection(method_name = :collection, klass: self, config: Config.new)
      collection = ClassCollection.new(klass, config)

      # @return [ClassCollection] `klass`を基底クラスとしたクラスコレクション
      define_singleton_method(method_name){ collection }

      if self.equal?(klass)
        extend CollectionDefinable.collection_module(collection)
        include InstanceMethods
      end
    end

    # インスタンスコレクションを返すメソッドを生成
    # @param [Symbol] method_name 生成するメソッド名
    # @param [Class] klass コレクションの基底クラス
    # @param [Config] config コレクションの設定オブジェクト
    def def_instance_collection(method_name = :collection, klass: self, config: Config.new)
      collection = InstanceCollection.new(klass, config)

      # @return [InstanceCollection] `klass`を基底クラスとしたインスタンスコレクション
      define_singleton_method(method_name){ collection }

      if self.equal?(klass)
        include(Module.new do

          # @return [String] キーを含むクラス名
          define_method(:class_name_with_key) do
            "#{collection.klass.name}[#{key}]"
          end
        end)
      end
    end

    # モジュールを格納するインスタンスコレクションを返すメソッドを生成
    # @param [Symbol] method_name 生成するメソッド名
    # @param [Config] config コレクションの設定オブジェクト
    def def_module_collection(method_name = :modules, config: Config.new)

      # @return [InstanceCollection<LocalModule>] モジュールを格納するインスタンスコレクション
      def_instance_collection method_name, klass: LocalModule, config: config
    end


    # @return [Module] 自身を基底クラスとしたコレクションメソッドを定義した場合に追加するクラスメソッド群
    def self.collection_module(collection)
      Module.new do

        # @return [String] キーを含むクラス名
        # @example
        #   member = MemberClass.inherits_to("hoge")
        #   member.name #=> "MemberClass[hoge]"
        define_method(:name_with_key) do
          if equal?(collection.klass) || name
            name
          elsif klass_name = collection.klass.name
            "#{klass_name}[#{key}]"
          end
        end

        alias_method :to_s, :name_with_key

        # @return [String] クラス名もしくはクラスオブジェクトの低レベル詳細
        def inspect
          name_with_key || super
        end

        # @return [Member] 自身もしくはキーを持っている最初の先祖
        define_method(:first_ancestor_with_key) do
          ancestors.find{|anc| anc.key || anc >= collection.klass }
        end
      end
    end

    # 自身を基底クラスとしたコレクションメソッドを定義した場合に追加するインスタンスメソッド群
    module InstanceMethods

      # @return [String] `::inspect`を含むインスタンスの低レベル詳細
      def inspect
        super.sub(/\A#<#<Class:\w+>/, "#<#{self.class.inspect}")
      end
    end
  end
end

module InheritableCollection
  class Config

    # パターンの中でキーに置換する箇所の正規表現
    KEY_PATTERN = /%\{key\}/.freeze

    # `@autoload_proc`のデフォルト値
    # 処理内容：最初に見つかった（Ruby）ファイルだけ読み込む
    # 処理を1度に限定するため`StopIteration`を発生させている
    DEFAULT_AUTOLOAD_PROC = ->(key, path){
      require(path)
      raise StopIteration
    }

    # @!attribute [rw] illegal_characters
    #   @return [Array<String>] 禁則文字の配列
    # @!attribute [rw] autoload_patterns
    #   @return [Array<String>] ライブラリを探索するパスのパターンの配列
    #   @note `Dir.glob`準拠のワイルドカードに対応
    # @!attribute [rw] autoload_proc
    #   @return [String] オートロード対象のファイルパスに対して行う処理
    attr_accessor :illegal_characters, :autoload_patterns, :autoload_proc

    def initialize
      self.illegal_characters = []
      self.autoload_patterns = []
      self.autoload_proc = DEFAULT_AUTOLOAD_PROC
    end

    def initialize_copy(obj)
      self.illegal_characters = obj.illegal_characters.dup
      self.autoload_patterns = obj.autoload_patterns.dup
    end

    # @param [String] key 検証対象のキー
    # @return [Array<String>] `key`に含まれる全ての禁則文字
    def illegal_characters_included_in(key)
      key = key.to_s
      illegal_characters.select{|chr| key.include?(chr) }
    end

    # @param [String] key 検証対象のキー
    # @raise [ArgumentError] `key`に禁則文字が含まれていた場合
    def validate_key(key)
      chrs = illegal_characters_included_in(key)
      raise ArgumentError, "key #{key.inspect} includes illegal character #{chrs}" if chrs.any?
    end

    # @return [Boolean] ライブラリ探索機能が有効かどうか
    def autoload?
      autoload_patterns.any?
    end

    # @param [String] key 検索するファイル名に含まれるキー
    # @return [Array<String>] パターン群にマッチするパスの配列
    # @note `@autoload_patterns`順・深さ優先・アルファベット順
    def autoload_paths(key = "*")
      Dir[*autoload_patterns.map{|pattern| pattern =~ KEY_PATTERN ? pattern.gsub(KEY_PATTERN, key) : File.join(pattern, key) }]
    end
  end
end

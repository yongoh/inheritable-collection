module InheritableCollection

  # クラスを格納するコレクション用メンバー
  # @note 今のところインスタンスメソッドは1つもないが、将来的に増えないとも限らないのでインクルード用モジュールにしておく
  module ClassMember
    def self.included(klass)
      klass.extend(ClassMethods)
      super(klass)
    end

    module ClassMethods
      include Member
      include ClassMethodDefinable
      include CollectionDefinable

      # このメンバー（クラス）を継承
      # @param [String,nil] key 作成するメンバーのキー
      # @return [Member] 継承して作成したメンバー（クラス）
      def inherits_to(key = nil, &block)
        Class.new(self) do
          self.key = key
          class_exec(&block) if block_given?
        end
      end

      private

      # @see ClassMethodDefinable#class_methods
      def class_methods(*args, &block)
        mod = super(*args, &block)
        extend mod
        mod
      end
    end
  end
end

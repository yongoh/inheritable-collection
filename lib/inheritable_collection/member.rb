module InheritableCollection
  module Member

    # @!attribute [r] key
    #   @return [String] コレクションから取得するためのキー
    attr_reader :key

    private

    # キーをセット
    def key=(key)
      @key = key.nil? ? nil : key.to_s.freeze
    end
  end
end

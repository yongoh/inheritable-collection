module InheritableCollection

  # インスタンスを格納するコレクション用メンバー
  module InstanceMember
    include Member

    def self.included(klass)
      klass.extend(CollectionDefinable)
      super(klass)
    end

    # @!attribute [r] superobject
    #   @return [Member] 継承元のメンバー（インスタンス）
    attr_reader :superobject

    # @param [String] key このメンバーのキー
    # @param [Member] superobject 継承元のメンバー（インスタンス）
    def initialize(key, superobject = nil)
      self.key = key
      @superobject = superobject
    end

    # このメンバー（インスタンス）を継承
    # @param [String,nil] key 作成するメンバーのキー
    # @return [Member] 継承して作成したメンバー（インスタンス）
    def inherits_to(key = nil, &block)
      Class.new(self.class, &block).new(key, self)
    end
  end
end

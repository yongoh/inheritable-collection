module InheritableCollection

  # インスタンスを格納するコレクションクラス
  # @note 基底クラスのインスタンスのみ格納可能
  class InstanceCollection < BasicCollection

    # メンバーを追加
    # @raise [TypeError] 基底クラスのインスタンス以外を追加しようとした場合に発生
    def <<(member)
      raise TypeError, "#{klass.name}のインスタンスでなければなりません。 #{member.inspect}" unless member.kind_of?(klass)
      super(member)
    end

    # 基底クラスをインスタンス化
    # @param [String] key 作成するメンバーのキー
    # @return [Member] 作成したメンバー
    def _build(key, *args, &block)
      klass.new(key, *args, &block)
    end

    # 基底クラスをインスタンス化してコレクションに加える
    # @param [String] key 作成するメンバーのキー
    # @return [Member] 作成したメンバー
    # @see #_build
    def build(key, *args, &block)
      self << _build(key, *args, &block)
    end

    # fromを継承
    # @param [String,nil] key 作成するメンバーのキー
    # @param [Member] from 継承元のメンバー（インスタンス）もしくはそのキー
    # @return [Member] 作成したメンバー
    def _inherit(key = nil, *args, from: nil, &block)
      if from
        self[from].inherits_to(key, &block)
      else
        Class.new(klass, &block).new(key, *args)
      end
    end
  end
end

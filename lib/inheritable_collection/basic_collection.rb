module InheritableCollection
  class BasicCollection
    extend Forwardable
    include Enumerable

    def_delegators :@data,
      :fetch,
      :size, :length, :empty?,
      :key, :index,
      :keys, :values, :values_at,
      :has_key?, :has_value?, :key?, :value?,
      :each_value, :each_key, :each_pair

    alias_method :each, :each_value
    alias_method :to_a, :values
    alias_method :to_ary, :values
    alias_method :include?, :has_value?
    alias_method :member?, :has_value?
    alias_method :===, :has_value?

    # @!attribute [r] klass
    #   @return [Class] 基底クラス。このクラスのインスタンスかサブクラスしかコレクションには入れられない。
    # @!attribute [r] default
    #   @return [Member] デフォルトで返すメンバー
    attr_reader :klass, :default

    # @!attribute [rw] config
    #   @return [Config] 設定オブジェクト
    attr_accessor :config

    # @param [Class] klass 基底クラス
    def initialize(klass, config = Config.new)
      @klass = klass
      @data = {}
      self.config = config
    end

    def initialize_copy(obj)
      @data = obj.instance_variable_get(:@data).dup
    end

    # @param [BasicCollection] other 比較対象のコレクション
    # @return [Boolean] `other`が同じ基底クラスとメンバー群を持っているかどうか
    # @note メンバー群の順番は不問
    def eql?(other)
      klass.eql?(other.klass) && values.sort_by(&:key).eql?(other.values.sort_by(&:key))
    end

    # メンバーを取得
    # @overload [](key)
    #   @param [String] key 取得したいメンバーのキー
    #   @return [Member] メンバー
    # @overload [](member)
    #   @param [klass] member コレクションに含まれるメンバー
    #   @return [Member] メンバー
    # @overload [](arg)
    #   @param [nil] arg nil
    #   @return [Member] デフォルトメンバー
    # @raise [KeyError] コレクションに含まれないキーを渡した場合に発生する
    def [](arg)
      if arg.nil?
        default
      elsif has_value?(arg)
        arg
      else
        if config.autoload? && !has_key?(arg.to_s)
          autoload(underscore(arg.to_s))
        end
        @data.fetch(arg.to_s)
      end
    end

    # メンバーを追加
    # @param [Member] member 追加するメンバー
    # @return [Member] 追加したメンバー
    # @note 追加する前にメンバーの検証を実施する
    def <<(member)
      config.validate_key(member.key)
      @data[member.key.to_s.freeze] = member
    end

    # メンバーを継承してコレクションに加える
    # @param [String] key 作成するメンバーのキー
    # @return [Member] 作成したメンバー
    def inherit(key, *args, &block)
      self << _inherit(key, *args, &block)
    end

    # @param [String] key 検索するファイル名に含まれるキー
    # @yield [path] 得られたファイルパスに対して行う処理
    # @yieldparam [String] path キーに対応するファイルのパス
    # @raise [LoadError] ファイルが一つも見つからなかった場合に発生
    def autoload(key, &block)
      block ||= config.autoload_proc

      paths = config.autoload_paths(key)
      raise LoadError, "cannot found file by such key -- #{key}" if paths.empty?
      paths.each do |path|
        begin
          instance_exec(key, path, &block)
        rescue StopIteration
          break
        end
      end
    end

    # 破壊的メソッド群
    %i(shift delete delete_if keep_if select! reject! clear).each do |method_name|
      define_method(method_name) do |*args, &block|
        @data.send(method_name, *args, &block)
        self
      end
    end

    # メンバー群を自身に追加
    # @param [Object] members 追加するメンバー群
    # @return [self] 変更後の自身
    # @note `members`は`#each`を、その中の各要素は`#key`を持っている必要がある
    def merge!(members)
      members.each do |member|
        self << member
      end
      self
    end

    # 自身のメンバー群を引数のメンバー群に置き換え
    # @param [Object] members 置き換えるメンバー群
    # @return [self] 変更後の自身
    def replace(members)
      clear.merge!(members)
    end

    # 和集合
    # @param [Object] members 演算相手のメンバー群
    # @return [BasicCollection] 新しいコレクション
    def union(members)
      dup.merge!(members)
    end

    alias_method :merge, :union
    alias_method :+, :union
    alias_method :|, :union

    # 差集合
    # @param [Object] members 演算相手のメンバー群
    # @return [BasicCollection] 新しいコレクション
    def difference(members)
      dup.replace(values - members)
    end

    alias_method :-, :difference

    # 共通部分
    # @param [Object] members 演算相手のメンバー群
    # @return [BasicCollection] 新しいコレクション
    def intersection(members)
      dup.replace(values & members)
    end

    alias_method :&, :intersection

    # 対称差
    # @param [Object] members 演算相手のメンバー群
    # @return [BasicCollection] 新しいコレクション
    def ^(members)
      dup.replace(values + members - (values & members))
    end

    # @return [Member] デフォルトメンバー
    # @note 初期状態では一番最初に追加したメンバー
    def default
      @default ||= self[keys.first]
    end

    # デフォルトメンバーをセット
    # @param [String,Member] arg 登録されているキーもしくはメンバー
    # @return [Member] デフォルトメンバー
    def default=(arg)
      @default = self[arg]
    end

    # @yield [config] 設定ブロック
    # @yieldparam [Config] config コレクション設定オブジェクト
    # @return [self] 自身
    def configure
      yield(config)
      self
    end

    # @return [Hash] `@data`のコピー
    def to_h
      @data.dup
    end

    alias_method :to_hash, :to_h

    private

    # @param [String] str キャメルケースの文字列
    # @return [String] スネークケースの文字列
    # @note ActiveSupportの`String#underscore`の再現
    def underscore(str)
      str.gsub(/([a-z])([A-Z])/){ [$1, $2.downcase].join('_') }.gsub(/[A-Z]/){|m| m.downcase }
    end
  end
end

module InheritableCollection
  module ClassMethodDefinable

    # @!attribute [rw] class_methods_modules
    #   @return [Hash<key => Module>] クラスメソッド用モジュールを格納したハッシュ
    attr_accessor :class_methods_modules

    def class_methods_modules
      @class_methods_modules ||= {}
    end

    private

    # ブロックの中でクラスメソッドを定義するメソッド（通常のmodule定義で記述するとトップレベルに定義されてしまうため）
    # @param [Object] mod_key 無名モジュールのキー
    # @return [Module] 定義したメソッド群を持つ無名モジュール
    def class_methods(mod_key, &block)
      class_methods_modules[mod_key] = Module.new(&block)
    end
  end
end

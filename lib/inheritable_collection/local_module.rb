module InheritableCollection
  class LocalModule < Module
    include InheritableCollection::Member
    include ClassMethodDefinable

    # @param [String] key コレクションから取得するためのキー
    # @yield [] ブロック内でメソッド定義等を行う
    # @yieldself [self] 自身
    def initialize(key = nil, &block)
      self.key = key
      class_eval(&block) if block_given?
    end

    # このモジュールをinclude時にクラスメソッドモジュールをextendする
    # @param [Class] klass include先のクラス
    def included(klass)
      class_methods_modules.each_value do |mod|
        klass.extend mod
      end
      super(klass)
    end

    # 親モジュールをinclude時に親モジュールで設定したクラスメソッドを引き継ぐ
    # @param [LocalModule] mod 親モジュール
    # @return [self] 自身
    def include(mod)
      class_methods_modules.merge!(mod.class_methods_modules) if mod.respond_to?(:class_methods_modules)
      super(mod)
    end
  end
end

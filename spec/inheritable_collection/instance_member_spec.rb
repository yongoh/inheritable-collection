require 'spec_helper.rb'

describe InheritableCollection::InstanceMember do
  let(:member_class){
    mod = described_class

    MemberInstance ||= Class.new do
      include mod

      attr_accessor :key
    end
  }

  let(:instance){ member_class.new("piyo") }


  describe "#inherits_to" do
    let(:result){ instance.inherits_to }

    it "は、子孫メンバーインスタンスを生成すること" do
      expect(result.key).to be_nil
      expect(result).to be_a(member_class)
      expect(result.class.superclass).to equal(member_class)
      expect(result.superobject).to equal(instance)
    end
  end

  describe "#inspect" do
    before do
      instance.instance_variable_set(:@aaa, 123)
      instance.instance_variable_set(:@bbb, "ABC")
      instance.instance_variable_set(:@ccc, 1..9)
    end

    it "は、インスタンスの低レベル詳細を返すこと" do
      regexp = /\A#<MemberInstance:\w{9} @key="piyo", @superobject=nil, @aaa=123, @bbb="ABC", @ccc=1..9>\z/
      expect(instance.inspect).to match(regexp)
    end
  end
end

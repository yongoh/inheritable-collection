require 'spec_helper.rb'

describe InheritableCollection::InstanceCollection do
  let(:member_class){ Class.new.include(InheritableCollection::InstanceMember) }
  let(:collection){ described_class.new(member_class) }

  let(:key){ "key" }

  shared_examples_for "build member" do
    it "は、渡したキーを持つ`collection.klass`のインスタンスを生成すること" do
      expect(result.key).to eq(key)
      expect(result).to be_instance_of(member_class)
    end
  end

  shared_examples_for "return instance" do
    it "は、渡したキーを持つメンバーインスタンスを生成すること" do
      expect(result.key).to eq(key)
      expect(result).to be_a(member_class)
      expect(result.class.superclass).to equal(member_class)
    end
  end


  describe "#_build" do
    let(:result){ collection._build(key) }

    it_behaves_like "build member"

    it "は、生成したインスタンスをコレクションに追加しないこと" do
      result
      expect(collection).not_to be_key(key)
      expect(collection).not_to have_value(result)
    end
  end

  describe "#build" do
    let(:result){ collection.build(key) }

    it_behaves_like "build member"

    it "は、生成したインスタンスをコレクションに追加すること" do
      result
      expect(collection).to be_key(key)
      expect(collection).to have_value(result)
    end
  end

  describe "#_inherit" do
    before do
      collection << parent
    end

    let(:parent){ collection.klass.new("piyo") }

    shared_examples_for "not add" do
      it "は、生成したインスタンスをコレクションに追加しないこと" do
        result
        expect(collection).not_to be_key(key)
        expect(collection).not_to have_value(result)
      end
    end


    context "キー文字列のみを渡した場合 (defoult)" do
      let(:result){ collection._inherit(key) }

      it{ expect(result.superobject).to be_nil }
      it_behaves_like "return instance"
      it_behaves_like "not add"
    end

    context "fromオプションにメンバーオブジェクトを渡した場合" do
      let(:result){ collection._inherit(key, from: parent) }

      it{ expect(result.superobject).to equal(parent) }
      it_behaves_like "return instance"
      it_behaves_like "not add"
    end

    context "fromオプションにすでにあるキーを渡した場合" do
      let(:result){ collection._inherit(key, from: parent.key) }

      it{ expect(result.superobject).to equal(parent) }
      it_behaves_like "return instance"
      it_behaves_like "not add"
    end

    context "fromオプションに`collection.klass`かそのサブクラスのインスタンス以外を渡した場合" do
      let(:object){ double("Object") }

      it "は、キーエラーを発生させること" do
        expect{ collection._inherit(key, from: object) }.to raise_error(KeyError, 'key not found: "#[Double \"Object\"]"')
      end
    end
  end

  describe "#inherit" do
    let(:result){ collection.inherit(key) }

    it_behaves_like "return instance"

    it "は、生成したインスタンスをコレクションに追加すること" do
      result
      expect(collection).to be_key(key)
      expect(collection).to have_value(result)
    end
  end

  describe "#<<" do
    context "`collection.klass`のインスタンスを渡した場合" do
      let(:member){ member_class.new(key) }

      it "は、渡したインスタンスを返すこと" do
        expect(collection << member).to equal(member)
      end

      it "は、渡したインスタンスをコレクションに追加すること" do
        collection << member

        expect(collection).to be_key(key)
        expect(collection).to have_value(member)
      end
    end

    context "`collection.klass`かそのサブクラスのインスタンスではないオブジェクトを渡した場合" do
      let(:object){ double("Other") }
      it{ expect{ collection << object }.to raise_error(TypeError, "#{member_class.name}のインスタンスでなければなりません。 #{object.inspect}") }
    end
  end
end

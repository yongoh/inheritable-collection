require 'spec_helper.rb'

describe InheritableCollection::ClassCollection do
  let(:member_class){ Class.new.include(InheritableCollection::ClassMember) }
  let(:collection){ described_class.new(member_class) }

  let(:key){ "hoge" }

  shared_examples_for "return inherited class" do
    it "は、渡したキーを持ち、`collection.klass`を継承したサブクラスを生成すること" do
      expect(result.key).to eq(key)
      expect(result.superclass).to equal(collection.klass)
    end
  end


  describe "#_inherit" do
    shared_examples_for "not add" do
      it "は、継承したクラスをコレクションに追加しないこと" do
        result
        expect(collection).not_to be_key(key)
        expect(collection).not_to have_value(result)
      end
    end


    context "キー文字列のみを渡した場合 (defoult)" do
      let(:result){ collection._inherit(key) }
      it_behaves_like "return inherited class"
      it_behaves_like "not add"
    end

    context "fromオプションに`collection.klass`のサブクラスを渡した場合" do
      let(:sub_class){ Class.new(member_class) }
      let(:result){ collection._inherit(key, from: sub_class) }

      it "は、渡したキーを持ち、渡したクラスを継承したサブクラスを生成すること" do
        expect(result.key).to eq(key)
        expect(result.superclass).to equal(sub_class)
      end

      it_behaves_like "not add"
    end

    context "fromオプションにすでにあるキーを渡した場合" do
      before do
        collection << member
      end

      let(:member){ Class.new(member_class){ self.key = "piyo" } }
      let(:result){ collection._inherit(key, from: member.key) }

      it "は、渡したキーを持ち、fromのキーを持つクラスを継承したサブクラスを生成すること" do
        expect(result.key).to eq(key)
        expect(result.superclass).to equal(member)
      end

      it_behaves_like "not add"
    end

    context "fromオプションに`collection.klass`のサブクラスではないクラスを渡した場合" do
      let(:klass){ Struct.new(:key) }

      it "は、キーエラーを発生させること" do
        expect{ collection._inherit(key, from: klass) }.to raise_error(KeyError, "key not found: \"#{klass.inspect}\"")
      end
    end
  end

  describe "#inherit" do
    let(:result){ collection.inherit(key) }

    it_behaves_like "return inherited class"

    it "は、継承したサブクラスをコレクションに追加すること" do
      result
      expect(collection).to be_key(key)
      expect(collection).to have_value(result)
    end
  end

  describe "#<<" do
    shared_examples_for "raise TypeError" do
      let(:msg){ "#{member_class.name}のサブクラスでなければなりません。 #{member.inspect}" }
      it{ expect{ collection << member }.to raise_error(TypeError, msg) }
    end


    context "メンバークラスのサブクラスを渡した場合" do
      before do
        allow(member).to receive(:key).and_return(key)
        collection << member
      end

      let(:member){ Class.new(member_class) }

      it "は、渡したものをコレクションに追加すること" do
        expect(collection[key]).to equal(member)
      end
    end

    context "メンバークラスのサブクラスではないクラスを渡した場合" do
      let(:member){ Class.new }
      it_behaves_like "raise TypeError"
    end

    context "クラスではないオブジェクトを渡した場合" do
      let(:member){ double("Other") }
      it_behaves_like "raise TypeError"
    end

    context "メンバークラスのインスタンスを渡した場合" do
      let(:member){ member_class.new }
      it_behaves_like "raise TypeError"
    end
  end
end

require 'spec_helper.rb'

describe InheritableCollection::ClassMember do
  let(:member_class){
    mod = described_class

    Class.new do
      include mod

      extend (Module.new do
        attr_accessor :key
      end)
    end
  }


  describe "::_inherit" do
    shared_examples_for "inheriting" do
      it "は、渡したキーを持つ自身のサブクラスを生成すること" do
        expect(result.key).to eq(key)
        expect(result.superclass).to eq(member)
      end
    end

    let(:key){ "piyo" }
    let(:result){ member.inherits_to(key) }

    context "klassの場合" do
      let(:member){ member_class }
      it_behaves_like "inheriting"
    end

    context "klassのサブクラスの場合" do
      let(:member){ Class.new(member_class){ self.key = "hoge" } }
      it_behaves_like "inheriting"
    end
  end

  describe "::class_methods" do
    let(:result){
      member_class.class_eval do
        class_methods "foobar" do
          def foo; "FOO" end
          def bar; "BAR" end
        end
      end
    }

    it "は、ブロック内で定義したメソッドを持つモジュールを返すこと" do
      expect(result).to be_a(Module)
      expect(result.instance_methods).to eq(%i(foo bar))
    end

    it "は、作成したモジュールを引数のキーで`#class_methods_modules`に追加すること" do
      result
      expect(member_class.class_methods_modules["foobar"]).to equal(result)
    end

    it "は、作成したモジュールを自身にextendすること" do
      expect(member_class).to be_a(result)
      expect(member_class.foo).to eq("FOO")
      expect(member_class.bar).to eq("BAR")
    end
  end
end

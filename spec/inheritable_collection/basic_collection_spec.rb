require 'spec_helper.rb'

describe InheritableCollection::BasicCollection do
  let(:member_class){ Struct.new(:key) }
  let(:member){ member_class.new("key") }
  let(:collection){ described_class.new(member_class) }

  before do
    collection << member
  end


  %i(dup clone).each do |method_name|
    describe "##{method_name}" do
      let(:result){ collection.send(method_name) }

      it "は、自身をコピーすること" do
        expect(result).to be_a(described_class)
        expect(result).not_to equal(collection)
      end

      it "は、`@data`もコピーすること" do
        data_1 = collection.instance_variable_get(:@data)
        data_2 = result.instance_variable_get(:@data)

        expect(data_2).to eql(data_1)
        expect(data_2).not_to equal(data_1)
      end

      it "は、`@config`をコピーせず元のオブジェクトと同じものを参照すること" do
        expect(result.config).to equal(collection.config)
      end

      it "は、`@default`をコピーせず元のオブジェクトと同じものを参照すること" do
        expect(result.default).to equal(collection.default)
      end
    end
  end

  describe "#eql?" do
    before do
      collection << arg_array[0]
      collection << arg_array[1]
    end

    context "同じ基底クラスと同じ順番のメンバー群を持つコレクションを渡した場合" do
      let(:other){ described_class.new(member_class) }

      before do
        other << member
        other << arg_array[0]
        other << arg_array[1]
      end

      it{ expect(collection).to eql(other) }
    end

    context "異なる基底クラスと同じ順番のメンバー群を持つコレクションを渡した場合" do
      let(:other){ described_class.new(Class.new(member_class)) }

      before do
        other << member
        other << arg_array[0]
        other << arg_array[1]
      end

      it{ expect(collection).not_to eql(other) }
    end

    context "同じ基底クラスと異なる順番のメンバー群を持つコレクションを渡した場合" do
      let(:other){ described_class.new(member_class) }

      before do
        other << arg_array[1]
        other << member
        other << arg_array[0]
      end

      it{ expect(collection).to eql(other) }
    end

    context "同じ基底クラスで自身には無いメンバーを含むコレクションを渡した場合" do
      let(:other){ described_class.new(member_class) }

      before do
        other << member
        other << arg_array[0]
        other << arg_array[1]
        other << arg_array[2]
      end

      it{ expect(collection).not_to eql(other) }
    end
  end

  describe "#<<" do
    context "同じキーを持つ別のメンバーを渡した場合" do
      before do
        collection << new_member
      end

      let(:new_member){ double("Member", key: member.key) }

      it "は、渡したメンバーで上書きすること" do
        expect(collection[member.key]).to equal(new_member)
        expect(collection).not_to have_value(member)
      end
    end

    context "禁則文字を含むキーを持つメンバーを渡した場合" do
      before do
        collection.configure do |config|
          config.illegal_characters << "a"
        end
      end

      let(:new_member){ member_class.new("abc") }

      it "は、引数エラーを発生させること" do
        expect{ collection << new_member }.to raise_error(ArgumentError, 'key "abc" includes illegal character ["a"]')
      end
    end
  end

  describe "#[]" do
    context "キーを渡した場合" do
      context "存在するキーの場合" do
        let(:key){ member.key }

        it "は、渡したキーを持つメンバーを返すこと" do
          expect(collection).to be_key(key)
          expect(collection[key]).to equal(member)
        end
      end

      context "存在しないキーの場合" do
        let(:key){ "foobar" }

        it "は、キーエラーを発生させること" do
          expect(collection).not_to be_key(key)
          expect{ collection[key] }.to raise_error(KeyError, "key not found: \"#{key}\"")
        end
      end
    end

    context "メンバークラスのインスタンスを渡した場合" do
      context "渡したインスタンスがコレクションに含まれる場合" do
        it "は、渡したインスタンスを返すこと" do
          expect(collection).to have_value(member)
          expect(collection[member]).to equal(member)
        end
      end

      context "渡したインスタンスがコレクションに含まれない場合" do
        let(:other){ member_class.new("hoge") }

        it "は、キーエラーを発生させること" do
          expect(collection).not_to have_value(other)
          expect{ collection[other] }.to raise_error(KeyError, 'key not found: "#<struct key=\"hoge\">"')
        end
      end
    end

    context "適当なオブジェクトを渡した場合" do
      let(:object){ double("Object") }

      it "は、キーエラーを発生させること" do
        expect{ collection[object] }.to raise_error(KeyError, 'key not found: "#[Double \"Object\"]"')
      end
    end

    context "nilを渡した場合" do
      it "は、デフォルトメンバーを返すこと" do
        expect(collection[nil]).to equal(collection.default)
      end
    end

    context "空文字を渡した場合" do
      let(:member){ double("Member", key: "") }

      it "は、空文字のキーを持つメンバーを返すこと" do
        expect(collection[""]).to equal(member)
      end
    end

    context "ライブラリ探索パターンを追加した場合" do
      before do
        collection.config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}.rb")

        $collection = collection
      end

      context "ファイルが存在するキーを渡した場合" do
        let(:key){ "hoge" }

        it "は、メンバー定義ファイルを読み込んでメンバーを返すこと" do
          expect(collection).not_to be_key(key)
          expect(collection[key].key).to eq(key)
          expect(collection).to be_key(key)
        end
      end

      context "ファイルが存在しないキーを渡した場合" do
        let(:key){ "piyo" }

        it "は、ロードエラーを発生させること" do
          expect(collection).not_to have_value(key)
          expect{ collection[key] }.to raise_error(LoadError, "cannot found file by such key -- piyo")
        end
      end
    end
  end

  describe "#default" do
    context "デフォルトメンバーを上書きしない場合" do
      it "は、最初に追加したメンバーをデフォルトとして返すこと" do
        expect(collection.default).to equal(member)
      end
    end

    context "デフォルトメンバーを上書きした場合" do
      before do
        collection << new_member
        collection.default = new_member
      end

      let(:new_member){ member_class.new("hoge") }

      it "は、上書きしたデフォルトメンバーを返すこと" do
        expect(collection.default).to equal(new_member)
      end

      it "は、上書きされたデフォルトメンバーを消さないこと" do
        expect(collection).to have_value(member)
      end
    end
  end

  describe "#autoload" do
    before do
      collection.config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}.rb")
    end

    context "ブロックを渡さない場合" do
      before do
        # 読み込みのテストのため、ロード済みファイルの一覧を毎回リセット
        re = /\A#{File.join(Dir.pwd, "resources")}/
        $LOADED_FEATURES.delete_if{|path| path =~ re }

        # テスト用ファイルの挙動確認用グローバル変数
        $collection = []
      end


      context "渡したキーと同じファイル名のRubyファイルがある場合" do
        it "は、Rubyファイル内の処理を確実に行うこと" do
          expect{ collection.autoload("baz") }.to change{ $collection }.from([]).to(["foo/baz"])
        end
      end

      context "渡したキーと同じファイル名のRubyファイルが複数ある場合" do
        it "は、最初にヒットしたファイルだけを読み込むこと" do
          expect{ collection.autoload("bar") }.to change{ $collection }.from([]).to(["aaa/bar"])
        end
      end

      context "'/'を含むキーを渡した場合" do
        it "は、'/'をパス区切り文字とみなしてヒットしたファイルだけを読み込むこと" do
          expect{ collection.autoload("foo/bar") }.to change{ $collection }.from([]).to(["foo/bar"])
        end
      end

      context "渡したキーと同じファイル名のRubyファイルが無い場合" do
        it "は、ロードエラーを発生させること" do
          expect{ collection.autoload("piyo") }.to raise_error(LoadError, "cannot found file by such key -- piyo")
        end
      end

      context "読み込んだファイル内で何らかの例外が発生した場合" do
        it "は、その例外を発生させること" do
          expect{ collection.autoload("error") }.to raise_error(RuntimeError, "error from -- resources/error.rb")
        end
      end
    end

    context "ブロックを渡した場合" do
      it "は、ヒットしたファイルの数だけ繰り返すこと" do
        expect{|b| collection.autoload("bar", &b) }.to yield_control.exactly(2)
      end

      it "は、渡したキーとヒットしたファイルパスをブロック内引数に渡すこと" do
        expect{|b| collection.autoload("bar", &b) }.to yield_successive_args(
          ["bar", File.join(Dir.pwd, "resources", "aaa", "bar.rb")],
          ["bar", File.join(Dir.pwd, "resources", "foo", "bar.rb")],
        )
      end

      let(:yieldself){ collection.autoload("bar"){ return self } }

      it "は、ブロック内の`self`にコレクション自身を割り当てること" do
        expect(yieldself).to equal(collection)
      end
    end
  end


  let(:arg_array){
    [
      double("Member", key: "key1"),
      double("Member", key: "key2"),
      double("Member", key: "key3"),
    ]
  }

  let(:arg_collection){
    c = described_class.new(member_class)
    c << double("Member", key: "key1")
    c << double("Member", key: "key2")
    c << double("Member", key: "key3")
    c
  }

  describe "破壊的メソッド群" do
    describe "#clear" do
      it "は、データを空にすること" do
        expect(collection).not_to be_empty
        collection.clear
        expect(collection).to be_empty
      end

      it "は、自身を返すこと" do
        expect(collection.clear).to equal(collection)
      end
    end

    describe "#merge!" do
      it "は、引数で渡されたメンバー群を自身に加えること" do
        expect{ collection.merge!(arg_array) }.to change(collection, :size).by(3)

        arg_array.each do |arg_member|
          expect(collection).to be_include(arg_member)
        end
      end

      it "は、自身を返すこと" do
        expect(collection.merge!(arg_array)).to equal(collection)
      end
    end

    describe "#replace" do
      shared_examples_for "replace members" do
        it "は、引数で渡されたメンバー群で自身の中身を置き換えること" do
          expect{ collection.replace(arg_array) }.to change(collection, :size).from(1).to(3)

          expect(collection).not_to be_include(member)
          arg_array.each do |arg_member|
            expect(collection).to be_include(arg_member)
          end
        end

        it "は、自身を返すこと" do
          expect(collection.replace(arg_array)).to equal(collection)
        end
      end


      context "ハッシュを渡した場合" do
        let(:arg_members){
          {
            "key1" => double("Member", key: "key1"),
            "key2" => double("Member", key: "key2"),
            "key3" => double("Member", key: "key3"),
          }
        }

        it "は、メソッド未定義エラーを発生させること" do
          expect{ collection.replace(arg_members) }.to raise_error(NoMethodError)
        end
      end

      context "配列を渡した場合" do
        it_behaves_like "replace members"
      end

      context "コレクションを渡した場合" do
        let(:arg_members){ arg_collection }
        it_behaves_like "replace members"
      end
    end
  end

  describe "集合系メソッド群" do
    shared_examples_for "non destructive" do
      it "は、自身を変化させないこと" do
        before_members = collection.to_a
        expect{ subject }.to change(collection, :size).by(0)
        expect(collection.to_a).to eql(before_members)
      end

      it "は、新しいコレクションを返すこと" do
        is_expected.to be_instance_of(described_class)
        is_expected.not_to equal(collection)
        is_expected.not_to equal(arg_members)
      end
    end


    describe "#union" do
      subject do
        collection.union(arg_members)
      end

      shared_examples_for "union members" do
        it "は、自身のメンバーと引数で渡されたメンバー群を合わせたコレクションを返すこと" do
          is_expected.to have_attributes(size: collection.size + arg_members.size)

          (collection.to_a + arg_members.to_a).each do |member|
            is_expected.to be_include(member)
          end
        end
      end


      context "配列を渡した場合" do
        let(:arg_members){ arg_array }
        it_behaves_like "non destructive"
        it_behaves_like "union members"
      end

      context "コレクションを渡した場合" do
        let(:arg_members){ arg_collection }
        it_behaves_like "non destructive"
        it_behaves_like "union members"
      end
    end

    describe "#difference" do
      subject do
        collection.difference(arg_members)
      end

      before do
        collection << arg_members.to_a[0]
        collection << arg_members.to_a[1]
      end

      shared_examples_for "difference members" do
        it "は、自身のメンバーから引数で渡されたメンバー群を除いたコレクションを返すこと" do
          is_expected.to have_attributes(size: 1)

          is_expected.to be_include(member)
          arg_members.each do |arg_member|
            is_expected.not_to be_include(arg_member)
          end
        end
      end


      context "配列を渡した場合" do
        let(:arg_members){ arg_array }
        it_behaves_like "non destructive"
        it_behaves_like "difference members"
      end

      context "コレクションを渡した場合" do
        let(:arg_members){ arg_collection }
        it_behaves_like "non destructive"
        it_behaves_like "difference members"
      end
    end

    describe "#intersection" do
      subject do
        collection.intersection(arg_members)
      end

      before do
        collection << arg_members.to_a[0]
        collection << arg_members.to_a[1]
      end

      shared_examples_for "intersection members" do
        it "は、自身のメンバーと引数で渡されたメンバーの共通部分だけを含んだコレクションを返すこと" do
          is_expected.to have_attributes(size: 2)
  
          is_expected.not_to be_include(member)
          is_expected.to be_include(arg_members.to_a[0])
          is_expected.to be_include(arg_members.to_a[1])
          is_expected.not_to be_include(arg_members.to_a[2])
        end
      end


      context "配列を渡した場合" do
        let(:arg_members){ arg_array }
        it_behaves_like "non destructive"
        it_behaves_like "intersection members"
      end

      context "コレクションを渡した場合" do
        let(:arg_members){ arg_collection }
        it_behaves_like "non destructive"
        it_behaves_like "intersection members"
      end
    end

    describe "#^" do
      subject do
        collection ^ arg_members
      end

      before do
        collection << arg_members.to_a[0]
        collection << arg_members.to_a[1]
      end

      shared_examples_for "^ members" do
        it "は、自身のメンバーと引数で渡されたメンバーのどちらか片方だけに含まれるメンバーを含んだコレクションを返すこと" do
          is_expected.to have_attributes(size: 2)
  
          is_expected.to be_include(member)
          is_expected.not_to be_include(arg_members.to_a[0])
          is_expected.not_to be_include(arg_members.to_a[1])
          is_expected.to be_include(arg_members.to_a[2])
        end
      end


      context "配列を渡した場合" do
        let(:arg_members){ arg_array }
        it_behaves_like "non destructive"
        it_behaves_like "^ members"
      end

      context "コレクションを渡した場合" do
        let(:arg_members){ arg_collection }
        it_behaves_like "non destructive"
        it_behaves_like "^ members"
      end
    end
  end

  describe "#to_h" do
    subject{ collection.to_h }

    it "は、`@data`のコピーを返すこと" do
      data = collection.instance_variable_get(:@data)

      is_expected.to eql(data)
      is_expected.not_to equal(data)
    end
  end
end

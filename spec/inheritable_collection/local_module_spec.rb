require 'spec_helper.rb'

describe InheritableCollection::LocalModule do
  describe "#class_methods" do
    let(:local_module){ described_class.new }

    let(:result){
      local_module.class_eval do
        class_methods "foobar" do
          def foo; "FOO" end
          def bar; "BAR" end
        end
      end
    }

    it "は、ブロック内で定義したメソッドを持つモジュールを返すこと" do
      expect(result).to be_instance_of(Module)
      expect(result.instance_methods).to eq(%i(foo bar))
    end

    it "は、作成したモジュールを引数のキーで`#class_methods_modules`に追加すること" do
      result
      expect(local_module.class_methods_modules["foobar"]).to equal(result)
    end
  end

  describe "インクルード" do
    let(:local_module){
      described_class.new do
        class_methods "foobar" do
          def foo; "FOO" end
          def bar; "BAR" end
        end

        def hoge; "ほげ" end
        def piyo; "ぴよ" end
      end
    }

    let(:instance){ klass.new }

    context "クラスにインクルードした場合" do
      let(:klass){ Class.new.send(:include, local_module) }

      it "は、クラスにクラスメソッドを付与すること" do
        expect(klass.foo).to eq("FOO")
        expect(klass.bar).to eq("BAR")
      end

      it "は、クラスにインスタンスメソッドを付与すること" do
        expect(instance.hoge).to eq("ほげ")
        expect(instance.piyo).to eq("ぴよ")
      end
    end

    context "モジュールにインクルードした場合" do
      let(:mod){ Module.new.send(:include, local_module) }
      let(:klass){ Class.new.send(:include, mod) }

      it "は、モジュールにモジュールメソッドを付与すること" do
        expect(mod.foo).to eq("FOO")
        expect(mod.bar).to eq("BAR")
      end

      it "は、モジュールにインスタンスメソッドを付与すること" do
        expect(mod.instance_methods).to eq(%i(hoge piyo))
      end

      it "は、モジュールをインクルードしたクラスにクラスメソッドを付与しないこと" do
        expect(klass).not_to be_respond_to(:foo)
        expect(klass).not_to be_respond_to(:bar)
      end

      it "は、モジュールをインクルードしたクラスにインスタンスメソッドを付与すること" do
        expect(instance.hoge).to eq("ほげ")
        expect(instance.piyo).to eq("ぴよ")
      end
    end

    context "`LocalModule`にインクルードした場合" do
      let(:other){
        mod = local_module

        described_class.new do
          include mod

          class_methods "baz" do
            def baz; "BAZ" end
          end
        end
      }

      let(:klass){ Class.new.send(:include, other) }

      it "は、`LocalModule`にモジュールメソッドを付与すること" do
        expect(other.foo).to eq("FOO")
        expect(other.bar).to eq("BAR")
      end

      it "は、`LocalModule`にインスタンスメソッドを付与すること" do
        expect(other.instance_methods).to eq(%i(hoge piyo))
      end

      it "は、`LocalModule`にクラスメソッドモジュール群を引き継ぐこと" do
        local_module.class_methods_modules.each do |key, mod|
          expect(other.class_methods_modules[key]).to equal(mod)
        end
      end

      it "は、`LocalModule`をインクルードしたクラスにクラスメソッドを付与すること" do
        expect(klass.foo).to eq("FOO")
        expect(klass.bar).to eq("BAR")
      end

      it "は、`LocalModule`をインクルードしたクラスにインスタンスメソッドを付与すること" do
        expect(instance.hoge).to eq("ほげ")
        expect(instance.piyo).to eq("ぴよ")
      end
    end
  end
end

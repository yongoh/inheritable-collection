require 'spec_helper.rb'

describe InheritableCollection::CollectionDefinable do
  let(:member_class){ Class.new.extend(described_class) }

  describe "#def_class_collection" do
    context "引数無しの場合" do
      before do
        member_class.class_eval do
          def_class_collection
        end
      end

      it "は、自身を基底クラスとしたクラスコレクションを返すメソッドを生成すること" do
        expect(member_class.collection).to be_a(InheritableCollection::ClassCollection)
        expect(member_class.collection.klass).to equal(member_class)
      end
    end

    context "第1引数にシンボルか文字列を渡した場合" do
      before do
        member_class.class_eval do
          def_class_collection :members
        end
      end

      it "は、渡した名前のクラスコレクションメソッドを生成すること" do
        expect(member_class.members).to be_a(InheritableCollection::ClassCollection)
        expect(member_class.members.klass).to equal(member_class)
        expect(member_class).not_to respond_to(:collection)
      end
    end

    context "オプション'klass'に別のクラスを渡した場合" do
      before do
        k = klass
        member_class.class_eval do
          def_class_collection klass: k
        end
      end

      let(:klass){ Class.new }

      it "は、渡したクラスを基底クラスとしたコレクションを返すメソッドを生成すること" do
        expect(member_class.collection).to be_a(InheritableCollection::ClassCollection)
        expect(member_class.collection.klass).to equal(klass)
      end
    end

    context "オプション'config'にコレクションの設定オブジェクトを渡した場合" do
      before do
        c = config
        member_class.class_eval do
          def_class_collection config: c
        end
      end

      let(:config){ InheritableCollection::Config.new }

      it "は、渡した設定オブジェクトを持つコレクションを返すメソッドを生成すること" do
        expect(member_class.collection).to be_a(InheritableCollection::ClassCollection)
        expect(member_class.collection.klass).to equal(member_class)
        expect(member_class.collection.config).to equal(config)
      end
    end
  end

  describe "#def_instance_collection" do
    context "引数無しの場合" do
      before do
        member_class.class_eval do
          def_instance_collection
        end
      end

      it "は、自身を基底クラスとしたインスタンスコレクションを返すメソッドを生成すること" do
        expect(member_class.collection).to be_a(InheritableCollection::InstanceCollection)
        expect(member_class.collection.klass).to equal(member_class)
      end
    end
  end

  describe "#def_module_collection" do
    context "引数無しの場合" do
      before do
        member_class.class_eval do
          def_module_collection
        end
      end

      it "は、モジュールを格納するインスタンスコレクションを返すメソッドを生成すること" do
        expect(member_class.modules).to be_a(InheritableCollection::InstanceCollection)
        expect(member_class.modules.klass).to equal(InheritableCollection::LocalModule)
      end
    end
  end

  describe "`#def_class_collection`後に追加されるメソッド群" do
    let(:member_class_1){
      mod = described_class

      Class.new do
        extend mod

        def_class_collection

        extend (Module.new do
          attr_accessor :key
        end)
      end
    }

    let(:sub_class_with_key_1){ Class.new(member_class_1){ self.key = "hoge" } }
    let(:sub_class_without_key_1){ Class.new(member_class_1) }


    MemberClass = Class.new do
      extend InheritableCollection::CollectionDefinable

      def_class_collection

      extend (Module.new do
        attr_accessor :key
      end)
    end

    SubMemberClass = Class.new(MemberClass){ self.key = "sub" }

    let(:sub_class_with_key_2){ Class.new(MemberClass){ self.key = "piyo" } }
    let(:sub_class_without_key_2){ Class.new(MemberClass) }


    describe "::name_with_key" do
      context "基底クラスが定数に配置されていない場合" do
        context "基底クラスの場合" do
          it{ expect(member_class_1.name_with_key).to be_nil }
        end

        context "キーを持つサブクラスの場合" do
          it{ expect(sub_class_with_key_1.name_with_key).to be_nil }
        end

        context "キーを持たないサブクラスの場合" do
          it{ expect(sub_class_without_key_1.name_with_key).to be_nil }
        end
      end

      context "基底クラスが定数に配置されている場合" do
        context "基底クラスの場合" do
          it "は、クラス名を返すこと" do
            expect(MemberClass.name_with_key).to eq("MemberClass")
          end
        end

        context "キーを持つサブクラスの場合" do
          it "は、キーを含むクラス名を返すこと" do
            expect(sub_class_with_key_2.name_with_key).to eq("MemberClass[piyo]")
          end
        end

        context "キーを持たないサブクラスの場合" do
          it "は、キー部分が空のクラス名を返すこと" do
            expect(sub_class_without_key_2.name_with_key).to eq("MemberClass[]")
          end
        end

        context "定数に配置されたサブクラスの場合" do
          it "は、クラス名を返すこと" do
            expect(SubMemberClass.name_with_key).to eq("SubMemberClass")
          end
        end
      end
    end

    describe "::inspect" do
      context "基底クラスが定数に配置されていない場合" do
        context "基底クラスの場合" do
          it{ expect(member_class_1.inspect).to match(/\A#<Class:\w+>\z/) }
        end

        context "キーを持つサブクラスの場合" do
          it{ expect(sub_class_with_key_1.inspect).to match(/\A#<Class:\w+>\z/) }
        end

        context "キーを持たないサブクラスの場合" do
          it{ expect(sub_class_without_key_1.inspect).to match(/\A#<Class:\w+>\z/) }
        end
      end

      context "基底クラスが定数に配置されている場合" do
        context "基底クラスの場合" do
          it "は、クラス名を返すこと" do
            expect(MemberClass.inspect).to eq("MemberClass")
          end
        end

        context "キーを持つサブクラスの場合" do
          it "は、キーを含むクラス名を返すこと" do
            expect(sub_class_with_key_2.inspect).to eq("MemberClass[piyo]")
          end
        end

        context "キーを持たないサブクラスの場合" do
          it "は、キー部分が空のクラス名を返すこと" do
            expect(sub_class_without_key_2.inspect).to eq("MemberClass[]")
          end
        end

        context "定数に配置されたサブクラスの場合" do
          it "は、クラス名を返すこと" do
            expect(SubMemberClass.inspect).to eq("SubMemberClass")
          end
        end
      end
    end

    describe "::first_ancestor_with_key" do
      context "基底クラスの場合" do
        it "は、自身を返すこと" do
          expect(member_class_1.first_ancestor_with_key).to equal(member_class_1)
        end
      end

      context "キーを持つサブクラスの場合" do
        it "は、自身を返すこと" do
          expect(sub_class_with_key_1.first_ancestor_with_key).to equal(sub_class_with_key_1)
        end
      end

      context "キーを持たないサブクラスの場合" do
        let(:sub_sub_class){ Class.new(sub_class_with_key_1) }

        it "は、キーを持つ最も近い先祖を返すこと" do
          expect(sub_sub_class.first_ancestor_with_key).to equal(sub_class_with_key_1)
        end
      end
    end

    describe "#inspect" do
      subject{ instance.inspect }

      before do
        instance.instance_variable_set(:@aaa, 123)
        instance.instance_variable_set(:@bbb, "ABC")
        instance.instance_variable_set(:@ccc, 1..9)
      end

      context "基底クラスが定数に配置されていない場合" do
        shared_examples_for "no class name" do
          it "は、クラスオブジェクトの詳細とインスタンスの詳細を返すこと" do
            is_expected.to match(/\A#<#<Class:\w+>:\w+ @aaa=123, @bbb="ABC", @ccc=1..9>\z/)
          end
        end


        context "基底クラスの場合" do
          let(:instance){ member_class_1.new }
          it_behaves_like "no class name"
        end

        context "キーを持つサブクラスの場合" do
          let(:instance){ sub_class_with_key_1.new }
          it_behaves_like "no class name"
        end

        context "キーを持たないサブクラスの場合" do
          let(:instance){ sub_class_without_key_1.new }
          it_behaves_like "no class name"
        end
      end

      context "基底クラスが定数に配置されている場合" do
        context "基底クラスの場合" do
          let(:instance){ MemberClass.new }

          it "は、クラス名とインスタンスの詳細を返すこと" do
            is_expected.to match(/\A#<MemberClass:\w+ @aaa=123, @bbb="ABC", @ccc=1..9>\z/)
          end
        end

        context "キーを持つサブクラスの場合" do
          let(:instance){ sub_class_with_key_2.new }

          it "は、キーを含むクラス名とインスタンスの詳細を返すこと" do
            is_expected.to match(/\A#<MemberClass\[piyo\]:\w+ @aaa=123, @bbb="ABC", @ccc=1..9>\z/)
          end
        end

        context "キーを持たないサブクラスの場合" do
          let(:instance){ sub_class_without_key_2.new }

          it "は、キー部分が空のクラス名インスタンスの詳細を返すこと" do
            is_expected.to match(/\A#<MemberClass\[\]:\w+ @aaa=123, @bbb="ABC", @ccc=1..9>\z/)
          end
        end

        context "定数に配置されたサブクラスの場合" do
          let(:instance){ SubMemberClass.new }

          it "は、クラス名とインスタンスの詳細を返すこと" do
            is_expected.to match(/\A#<SubMemberClass:\w+ @aaa=123, @bbb="ABC", @ccc=1..9>\z/)
          end
        end
      end
    end
  end

  describe "`#def_instance_collection`後に追加されるメソッド群" do
    let(:member_class){
      mod = described_class

      Class.new do
        extend mod

        def_instance_collection

        def self.name
          "MemberClass"
        end

        def key
          "hoge"
        end
      end
    }

    let(:member){ member_class.new }

    describe "#class_name_with_key" do
      it "は、キーを含むクラス名を返すこと" do
        expect(member.class_name_with_key).to eq("MemberClass[hoge]")
      end
    end
  end
end

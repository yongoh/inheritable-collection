require 'spec_helper.rb'

describe InheritableCollection::Config do
  let(:config){ described_class.new }


  %i(dup clone).each do |method_name|
    describe "##{method_name}" do
      let(:result){ config.send(method_name) }

      it "は、自身をコピーすること" do
        expect(result).to be_a(described_class)
        expect(result).not_to equal(config)
      end

      %i(illegal_characters autoload_patterns).each do |item|
        var = "@#{item}"

        it "は、`#{var}`もコピーすること" do
          config_item = config.instance_variable_get(var)
          result_item = result.instance_variable_get(var)

          expect(result_item).to eql(config_item)
          expect(result_item).not_to equal(config_item)
        end
      end
    end
  end

  describe "キー検証系メソッド群" do
    before do
      config.illegal_characters = %w(d f)
    end

    let(:valid_key){ "abc" }
    let(:invalid_key){ "def" }

    describe "#illegal_characters_included_in" do
      context "禁則文字を含まないキーを渡した場合" do
        it "は、空配列を返すこと" do
          expect(config.illegal_characters_included_in(valid_key)).to eq([])
        end
      end

      context "禁則文字を含むキーを渡した場合" do
        it "は、引っかかった禁則文字の配列を返すこと" do
          expect(config.illegal_characters_included_in(invalid_key)).to eq(["d", "f"])
        end
      end
    end

    describe "#validate_key" do
      context "禁則文字を含まないキーを渡した場合" do
        example do
          expect{ config.validate_key(valid_key) }.not_to raise_error
        end
      end

      context "禁則文字を含むキーを渡した場合" do
        example do
          expect{
            config.validate_key(invalid_key)
          }.to raise_error(ArgumentError, 'key "def" includes illegal character ["d", "f"]')
        end
      end
    end
  end

  describe "#autoload_paths" do
    context "'%{key}'を含まないパターンを持っている場合" do
      context "拡張子を持たないパターンの場合" do
        before do
          config.autoload_patterns << File.join(Dir.pwd, "resources", "**")
        end

        context "キーを渡さない場合" do
          it "は、パターンにヒットするパス（ディレクトリパス含む）の配列を返すこと" do
            expect(config.autoload_paths).to eq([
              File.join(Dir.pwd, "resources", "aaa"),
              File.join(Dir.pwd, "resources", "aaa", "bar.rb"),
              File.join(Dir.pwd, "resources", "bar"),
              File.join(Dir.pwd, "resources", "error.rb"),
              File.join(Dir.pwd, "resources", "foo"),
              File.join(Dir.pwd, "resources", "foo", "bar.rb"),
              File.join(Dir.pwd, "resources", "foo", "baz.rb"),
              File.join(Dir.pwd, "resources", "foo", "spam"),
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.rb"),
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.txt"),
              File.join(Dir.pwd, "resources", "hoge.rb"),
            ])
          end
        end

        context "複数のディレクトリにファイルがあるキーを渡した場合" do
          it "は、ファイル名が渡したキーと等しいファイルパスの配列を返すこと" do
            expect(config.autoload_paths("bar")).to eq([
              File.join(Dir.pwd, "resources", "bar"),
            ])
          end
        end

        context "複数の拡張子のファイルがあるキーを渡した場合" do
          it{ expect(config.autoload_paths("ham")).to eq([]) }
        end
      end

      context "拡張子を持つパターンの場合" do
        before do
          config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "*.*")
        end

        context "キーを渡さない場合" do
          it{ expect(config.autoload_paths).to eq([]) }
        end

        context "複数のディレクトリにファイルがあるキーを渡した場合" do
          it{ expect(config.autoload_paths("bar")).to eq([]) }
        end

        context "複数の拡張子のファイルがあるキーを渡した場合" do
          it{ expect(config.autoload_paths("ham")).to eq([]) }
        end
      end
    end

    context "'%{key}'を含つパターンを持っている場合" do
      context "拡張子を持たないパターンの場合" do
        before do
          config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}")
        end

        context "キーを渡さない場合" do
          it "は、'%{key}'をワイルドカードに置換したパターンにヒットするパス（ディレクトリパス含む）の配列を返すこと" do
            expect(config.autoload_paths).to eq([
              File.join(Dir.pwd, "resources", "aaa"),
              File.join(Dir.pwd, "resources", "aaa", "bar.rb"),
              File.join(Dir.pwd, "resources", "bar"),
              File.join(Dir.pwd, "resources", "error.rb"),
              File.join(Dir.pwd, "resources", "foo"),
              File.join(Dir.pwd, "resources", "foo", "bar.rb"),
              File.join(Dir.pwd, "resources", "foo", "baz.rb"),
              File.join(Dir.pwd, "resources", "foo", "spam"),
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.rb"),
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.txt"),
              File.join(Dir.pwd, "resources", "hoge.rb"),
            ])
          end
        end

        context "複数のディレクトリにファイルがあるキーを渡した場合" do
          before do
            config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}.rb")
          end

          it "は、ファイル名が渡したキーと等しいファイルパスの配列を返すこと" do
            expect(config.autoload_paths("bar")).to eq([
              File.join(Dir.pwd, "resources", "bar"),
              File.join(Dir.pwd, "resources", "aaa", "bar.rb"),
              File.join(Dir.pwd, "resources", "foo", "bar.rb"),
            ])
          end
        end

        context "複数の拡張子のファイルがあるキーを渡した場合" do
          before do
            config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}.txt")
          end

          it "は、'%{key}'を渡したキーに置換したパターンにヒットするパスの配列を返すこと" do
            expect(config.autoload_paths("ham")).to eq([
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.txt"),
            ])
          end
        end
      end

      context "拡張子を持つパターンの場合" do
        before do
          config.autoload_patterns << File.join(Dir.pwd, "resources", "**", "%{key}.rb")
        end

        context "キーを渡さない場合" do
          it "は、'%{key}'をワイルドカードに置換したパターンにヒットするパスの配列を返すこと" do
            expect(config.autoload_paths).to eq([
              File.join(Dir.pwd, "resources", "aaa", "bar.rb"),
              File.join(Dir.pwd, "resources", "error.rb"),
              File.join(Dir.pwd, "resources", "foo", "bar.rb"),
              File.join(Dir.pwd, "resources", "foo", "baz.rb"),
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.rb"),
              File.join(Dir.pwd, "resources", "hoge.rb"),
            ])
          end
        end

        context "複数のディレクトリにファイルがあるキーを渡した場合" do
          it "は、'%{key}'を渡したキーに置換したパターンにヒットするパスの配列を返すこと" do
            expect(config.autoload_paths("bar")).to eq([
              File.join(Dir.pwd, "resources", "aaa", "bar.rb"),
              File.join(Dir.pwd, "resources", "foo", "bar.rb"),
            ])
          end
        end

        context "複数の拡張子のファイルがあるキーを渡した場合" do
          it "は、'%{key}'を渡したキーに置換したパターンにヒットするパスの配列を返すこと" do
            expect(config.autoload_paths("ham")).to eq([
              File.join(Dir.pwd, "resources", "foo", "spam", "ham.rb"),
            ])
          end
        end
      end
    end
  end
end
